#!/usr/bin/env bash
# Copyright (C) 2019, Marcos Moreira <marcosml38@protonmail.ch>

# Variável de comando
versaoOpenssl=$(openssl version)

# Coloração de destaque
corVerde="\\e[0;42m"
corFinal="\\e[0m"

# Função de ajuda
function exibirAjuda()
{
	echo "certificado-ssl.sh [parâmetro]"
	echo "-a	Cria uma autoridade de certificação (AC)."
	echo "-c	Assina um certificado SSL com uma AC."
	echo "-v	Exibe a versão atual do OpenSSL."
}

# Checagem de requisitos
if ! [[ $(which openssl) ]]; then
	echo "Utilitário 'OpenSSL' não encontrado."
	exit 1
elif [[ -z "$1" ]]; then
	exibirAjuda
	exit 1
fi

# Entrada de parâmetros
case "$1" in
	-a)
		openssl genrsa -out Autoridade.key 4096
		openssl req -x509 -new -nodes -key Autoridade.key -sha512 -days 3650 -out Autoridade.crt
		echo -e "Sua chave é ${corVerde}Autoridade.key${corFinal} e seu certificado é ${corVerde}Autoridade.crt${corFinal}."
	;;
	-c)
		if ! [[ -e configuracao.txt ]]; then
			echo "Arquivo de configuração não encontrado."
			exit 1
		elif ! [[ -e Autoridade.key ]]; then
			echo "Chave da AC não encontrada."
			exit 1
		elif ! [[ -e Autoridade.crt ]]; then
			echo "Certificado da AC não encontrado."
			exit 1
		fi
		openssl genrsa -out Certificado.key 4096
		openssl req -new -key Certificado.key -out Certificado.csr
		openssl x509 -req -in Certificado.csr		\
			-CA Autoridade.crt 			\
			-CAkey Autoridade.key			\
			-CAcreateserial -out Certificado.crt 	\
			-sha512					\
			-days 365				\
			-extfile configuracao.txt
		echo -e "Sua chave é ${corVerde}Certificado.key${corFinal} e seu certificado é ${corVerde}Certificado.crt${corFinal}."
	;;
	-v)
		echo "Versão do OpenSSL: ${versaoOpenssl}"
	;;
	*)
		exibirAjuda
	;;
esac
